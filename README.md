# CypA datasets
__author__: Jordi Juarez-Jimenez, PhD

### University of Edinburgh 2018-2019
======================================

This repository contains datasets linked to our work on CypA dynamics.

__ensemble_pdbs:__ contains 100 pdb structures for each of the 100 microstates for CypA, D66A and H70A in three different compressed tar files

__MSM_files:__ contains the static distribution and the transition matrix for each one of the CypA, D66A and H70A models as a python pickle. 

__input_files:__ contains example input files to perform the accelerated MD and seeded MD trajectories.

__CypA_MSM.mp4:__ Video summarizing the conformational changes identified in the MSM.